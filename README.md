# ComfyLine - a powerline in YOUR control

A highly customizable zsh theme for your terminal, which is stupidly easy to configure. Just follow the below documentation and it will guide you through!

## Prerequisites and Installation

- Either [Powerline fonts](https://github.com/powerline/fonts) (Patched version of common fonts) or [Nerdfonts](https://nerdfonts.com) are mandatory to render the prompt properly. Do change your default terminal fonts. 

- You will optionally need [oh-my-zsh](https://ohmyz.sh), and if you want to use a detailed battery prompt use the oh-my-zsh [battery plugin](https://github.com/ohmyzsh/ohmyzsh/tree/master/plugins/battery), just include it in the plugins section. The prompt by default consists a minimal battery function which has less features but just works on most distros.

- It is certainly possible to use this prompt without oh-my-zsh but some of the git icons won't work well

- Finally follow through installer ( run install.sh using zsh ) or just manually source the `.zsh-theme` file in your .zshrc.

---
 
## Screenshots

<figure align="center">
  <img src="./screenshots/1.jpg" alt="">
  <br>
  <figcaption><b>screenshot-1:</b> Comfyline default theme</figcaption>
</figure>

---

<figure align="center">
  <img src="./screenshots/2.jpg" alt="">
  <br>
  <figcaption><b>screenshot-2:</b> Comfyline agnoster like theme</figcaption>
</figure>

---

<figure align="center">
  <kbd>
    <img src="./screenshots/3.jpg" alt="" style="border: 0.5px solid black" >
  </kbd>
  <br>
  <figcaption><b>screenshot-3:</b> Comfyline default theme - With battery segment,</figcaption>
  <figcaption> no oh-my-zsh, and next line character set </figcaption>
</figure>

---

## Configuration Variables

The prompt can be easily configured, by using various variables. Just set them anywhere **before** loading oh-my-zsh in .zshrc (mostly some thing like 'source .../oh-my-zsh.sh' ) so that the theme can read these variables.

Here are the all the list of variables you can play around with.

### Segment separators

You can the change the unicode character that acts as a segment separator in the powerline.

```{.zsh}
COMFYLINE_SEGSEP='\ue0b4'   # semicircle use \ue0b0 for traingle, looks bettery
COMFYLINE_SEGSEP_REVERSE='\ue0b6'  # same thing reverse, use \ue0b2 for reverse triangle
COMFYLINE_NO_START=1      # variable to have no segment seperator at the start and end of the powerline from the edge of terminal
```

### Rank

Rank is just the relative horizontal position calculated from left to right ( 1, 2, 3 ... ), with ( -1, -2 ...) denoting segments on the right from right to left, to hide them just put 0 as position. All you have to do is to change the number of the rank, the defaults are below

```{.zsh}
RETVAL_RANK=1  # return value of previous command if error then displays it in red
HOST_RANK=2    # hostname
USER_RANK=3
DIR_RANK=4
GIT_RANK=5
VENV_RANK=6
BAT_RANK=-1
DATE_RANK=-2
TIME_RANK=-3
```

### Colors

Every element's background and foreground color can be changed by setting the following variables:

Here are the defaults for the light theme:

```{.zsh}
RETVAL_b="#8a8bd8"    # background
RETVAL_f="#61355c"    # foreground
HOST_b="#b3b5fb"
HOST_f="#4a4b87"
USER_b="#f8bbe5"
USER_f="#874c80"
GIT_b="#f6b3b3"
GIT_f="#d95353"
GIT_CLEAN_b="#b3f58c"
GIT_CLEAN_f="#568459"
DIR_b="#e1bff2"
DIR_f="#844189"
VENV_b="#a8ddf9"
VENV_f="#0066a4"
BAT_b="#8a8bd8"
BAT_f="#61355c"
DATE_b="#b3b5fb"
DATE_f="#4a4b87"
TIME_b="#f8bbe5"
TIME_f="#874c80"
```

## Battery Variables

The theme file as mentioned contains a minimal version of battery plugin as mentioned which, only gives the status and percentage.
But that doesn't mean you can't play around with it, here are all the variables and their defaults.

The below icons are taken from [starship](https://starship.rs/config/#battery) ( if they don't render for you in this readme )

```{.zsh}
COMFYLINE_BATTERY_LOW=15      # Below this low charge icon is displayed
COMFYLINE_BATTERY_HIGH=90     # Above this high charge icon is displayed
COMFYLINE_CHARGING_ICON="⚡️"
COMFYLINE_HIGHCHARGE_ICON="󰁹"
COMFYLINE_MIDCHARGE_ICON="󰁽"
COMFYLINE_LOWCHARGE_ICON="󰂃"

# finally  this will use the battery plugin of oh-my-zsh, if you have it installed
COMFYLINE_BATTERY_PLUGIN=1
```

## Git variables

One limitation of comfyline is that, you can completely use this prompt without oh-my-zsh, but the git icons don't work the same. I use something called `ZSH_THEME_GIT_PROMPT` variables which are provided by oh-my-zsh [here](https://github.com/ohmyzsh/ohmyzsh/blob/master/lib/git.zsh).

If you want to change the icons or text, you have to change the `ZSH_THEME_GIT_PROMPT` variables in the `comfyline.zsh-theme` file manually.

### Miscellaneous

There are other variables too which can played around with

- `COMFYLINE_FULL_DIR`: which uses absolute paths instead of relative. Set it to 1 to activate it
- `COMFYLINE_FULL_HOSTNAME`: uses the expanded version of hostname ( with machine name ).

- `COMFYLINE_DATE_FORMAT`: Set the format of the date segment. It is a string whose format is same as the `date` command in UNIX. The default is
- `COMFYLINE_TIME_FORMAT`: Set the format of the time segment. Same format as above.

- `COMFYLINE_START_NEXT_LINE`: When set to 1, the cursor will begin in the next line. You can set a custom starting character which will be before the typing area using the `COMFYLINE_NEXT_LINE_CHAR` variable. Default is empty. There is also `COMFYLINE_NEXT_LINE_CHAR_COLOR` variable to set the color of the character
- `COMFYLINE_NO_GAP_LINE`: When set to 1, the prompt looks more continous, by default the prompt takes up 2 lines, now it will be condensed to .

- `COMFYLINE_RETVAL_NUMBER`: Specifies the type of return values of the previous command execution.
  - If set to 1, it gives the number only
  - If set to 2, it gives the number with error symbol, only if it is not 0.
  - If set to anything else or by default, only gives error symbol when return value is not 0, otherwise nothing.

## Contact Info

Feel free to present any ideas, feature requests or contact me:
email: atp@tutamail.com
xmpp: notpua@jappix.com 
