#!/usr/bin/zsh
# installer for the theme, run it using zsh is ./install.sh doesn't work

# finding zshrc location
if [[ -f ~/.zshrc ]]; then
    ZSHRC="$HOME/.zshrc"    
    echo "Found .zshrc in $ZSHRC"
elif [[ $ZDOTDIR != "" ]]; then
    ZSHRC="$ZDOTDIR/.zshrc"
    echo "Found .zshrc in $ZSHRC"
else
    echo "Couldn't find .zshrc manually add theme by setting the ZSH_THEME variable or sourcing the .zsh-theme file"
fi

# some people like to have symbolic links.
if [[ -L "$ZSHRC" ]]; then
    ZSHRC="$(readlink -f $ZSHRC)"
    echo "Found .zshrc symlinked to $ZSHRC"
fi

if [[ $ZSH == "" ]]; then
    if [[ -d ~/.oh-my-zsh ]]; then
        echo "Found Oh-My-Zsh adding theme"
        cp ./comfyline.zsh-theme ~/.oh-my-zsh/custom/themes/
        sed -i 's/ZSH_THEME="[^\"]*"/ZSH_THEME="comfyline"/' $ZSHRC
    else 
        echo "Couldn't find Oh-my-zsh adding theme without oh-my-zsh, manually sourcing the current folder (some features may not work)"
        echo "source $(pwd)/comfyline.zsh-theme" >> $ZSHRC
    fi
else
    echo "Found Oh-My-Zsh adding theme"
    cp ./comfyline.zsh-theme $ZSH/custom/themes/
    if [[ $(grep -i 'ZSH_THEME' $ZSHRC) == "" ]]; then
        echo "Seems like you don't have any theme set. Creating variable."
        sed -i '1s/^/ZSH_THEME="comfyline"\n/' $ZSHRC
    else
        sed -i 's/ZSH_THEME="[^\"]*"/ZSH_THEME="comfyline"/' $ZSHRC
    fi
fi 

echo "Installation complete, check if all variables are set in your .zshrc"
